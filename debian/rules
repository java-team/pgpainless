#!/usr/bin/make -f

include /usr/share/javahelper/java-vars.mk
include /usr/share/dpkg/pkg-info.mk

export JAVA_TOOL_OPTIONS=-Dfile.encoding=UTF-8

TEST_CORE_CLASSPATH=/usr/share/java/bcpg.jar:/usr/share/java/slf4j-api.jar:/usr/share/java/slf4j-simple.jar:pgpainless-core/build/libs/pgpainless-core.jar
TEST_SOP_CLASSPATH=$(TEST_CORE_CLASSPATH):/usr/share/java/sop-java.jar:/usr/share/java/sop-java-testfixtures.jar:pgpainless-sop/build/libs/pgpainless-sop.jar
TEST_CLI_CLASSPATH=$(TEST_SOP_CLASSPATH):/usr/share/java/sop-java-picocli.jar:/usr/share/java/junit5-system-exit.jar:pgpainless-cli/build/libs/pgpainless-cli.jar
JUNIT_OPTIONS=--exclude-classname="^sop\.testsuite\.\S+" --scan-classpath --fail-if-no-tests --disable-ansi-colors

BUILD_TASKS = jar

ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS) $(DEB_BUILD_PROFILES)))
	BUILD_TASKS += javadoc
endif

ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	BUILD_TASKS += testJar
endif

%:
	dh $@ --buildsystem=gradle --with javahelper,maven_repo_helper

override_dh_auto_build:
	dh_auto_build -- $(BUILD_TASKS)

execute_after_dh_auto_build:
	for n in pgpainless-core pgpainless-sop pgpainless-cli; do \
		cd $$n/build/libs; \
		ln -sf $$n-$(DEB_VERSION_UPSTREAM).jar $$n.jar; \
		test -e $$n-$(DEB_VERSION_UPSTREAM)-tests.jar && \
		ln -sf $$n-$(DEB_VERSION_UPSTREAM)-tests.jar $$n-tests.jar; \
		cd $(CURDIR); \
	done

override_dh_auto_test:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	java \
		-Djava.security.manager=allow \
		-jar /usr/share/java/junit-platform-console-standalone.jar execute \
		-cp $(TEST_CORE_CLASSPATH):pgpainless-core/build/libs/pgpainless-core-tests.jar \
		$(JUNIT_OPTIONS)
	java \
		-Djava.security.manager=allow \
		-jar /usr/share/java/junit-platform-console-standalone.jar execute \
		-cp $(TEST_SOP_CLASSPATH):pgpainless-sop/build/libs/pgpainless-sop-tests.jar \
		$(JUNIT_OPTIONS)
	java -Djava.security.manager=allow \
		-Xbootclasspath/a:/usr/share/java/sop-java-picocli.jar \
		-jar /usr/share/java/junit-platform-console-standalone.jar execute \
		-cp $(TEST_CLI_CLASSPATH):pgpainless-cli/build/libs/pgpainless-cli-tests.jar \
		$(JUNIT_OPTIONS)
endif

override_dh_install:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS $(DEB_BUILD_PROFILES))))
	dh_install
else
	dh_install -Nlibpgpainless-core-java-doc -Nlibpgpainless-cli-java-doc -Nlibpgpainless-sop-java-doc
endif
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	dh_install -p libpgpainless-core-java pgpainless-core/build/libs/pgpainless-core-*tests.jar /usr/share/java
	dh_install -p libpgpainless-sop-java pgpainless-sop/build/libs/pgpainless-sop-*tests.jar /usr/share/java
	dh_install -p libpgpainless-cli-java pgpainless-cli/build/libs/pgpainless-cli-*tests.jar /usr/share/java
endif

override_dh_installman:
ifeq (,$(filter nodoc,$(DEB_BUILD_OPTIONS) $(DEB_BUILD_PROFILES)))
	dh_installman -p pgpainless-cli pgpainless-cli/packaging/man/*
endif

execute_after_jh_manifest:
ifeq (,$(filter nocheck,$(DEB_BUILD_OPTIONS)))
	jh_manifest -c "junit5-system-exit.jar bcutil.jar pgpainless-cli.jar" debian/libpgpainless-cli-java/usr/share/java/pgpainless-cli-tests.jar
	jh_manifest -c "bcutil.jar pgpainless-core.jar" debian/libpgpainless-core-java/usr/share/java/pgpainless-core-tests.jar
	jh_manifest -c "pgpainless-sop.jar" debian/libpgpainless-sop-java/usr/share/java/pgpainless-sop-tests.jar
endif
