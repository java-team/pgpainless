Source: pgpainless
Section: java
Priority: optional
Maintainer: Debian Java Maintainers <pkg-java-maintainers@lists.alioth.debian.org>
Uploaders:
 Jérôme Charaoui <jerome@riseup.net>,
Build-Depends:
 debhelper-compat (= 13),
 default-jdk,
 default-jdk-doc <!nodoc>,
 gradle-debian-helper,
 javahelper,
 junit5 <!nocheck>,
 libbcpg-java (>= 1.77),
 libjsr305-java,
 libjunit5-system-exit-java <!nocheck>,
 libslf4j-java,
 libsop-java-java (>= 7.0.1),
 libsop-java-picocli-java (>= 7.0.1),
 maven-debian-helper,
 maven-repo-helper,
Standards-Version: 4.7.0
Vcs-Git: https://salsa.debian.org/java-team/pgpainless.git
Vcs-Browser: https://salsa.debian.org/java-team/pgpainless
Homepage: https://pgpainless.org/
Rules-Requires-Root: no

Package: libpgpainless-core-java
Architecture: all
Depends:
 libbcpg-java (>= 1.77),
 libjsr305-java,
 libslf4j-java,
 ${maven:Depends},
 ${misc:Depends},
Description: Easy to use OpenPGP library
 PGPainless is a modern implementation of OpenPGP in Java. It offers an
 ergonomic interface on top of the BouncyCastle cryptographic toolkit
 that makes it straightforward to deal with OpenPGP data.
 .
 This package contains the core PGPainless classes.

Package: libpgpainless-core-java-doc
Build-Profiles: <!nodoc>
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 libjs-jquery,
 libjs-jquery-ui,
 ${misc:Depends},
Description: Easy to use OpenPGP library - docs
 PGPainless is a modern implementation of OpenPGP in Java. It offers an
 ergonomic interface on top of the BouncyCastle cryptographic toolkit
 that makes it straightforward to deal with OpenPGP data.
 .
 This package contains documentation for libpgpainless-core-java.

Package: libpgpainless-sop-java
Architecture: all
Depends:
 libjsr305-java,
 libpgpainless-core-java (= ${source:Version}),
 libsop-java-java (>= 7.0.1),
 ${maven:Depends},
 ${misc:Depends},
Description: Stateless OpenPGP Protocol with PGPainless - library
 PGPainless is a modern implementation of OpenPGP in Java. It offers an
 ergonomic interface on top of the BouncyCastle cryptographic toolkit
 that makes it straightforward to deal with OpenPGP data.
 .
 This package contains the PGPainless-SOP implementation classes.

Package: libpgpainless-sop-java-doc
Build-Profiles: <!nodoc>
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
Description: Stateless OpenPGP Protocol with PGPainless - library - docs
 PGPainless is a modern implementation of OpenPGP in Java. It offers an
 ergonomic interface on top of the BouncyCastle cryptographic toolkit
 that makes it straightforward to deal with OpenPGP data.
 .
 This package contains documentation for libpgpainless-sop-java.

Package: libpgpainless-cli-java
Architecture: all
Depends:
 libjsr305-java,
 libpgpainless-sop-java (= ${source:Version}),
 libsop-java-picocli-java (>= 7.0.1),
 ${maven:Depends},
 ${misc:Depends},
Description: Stateless OpenPGP Protocol with PGPainless - runtime library
 PGPainless is a modern implementation of OpenPGP in Java. It offers an
 ergonomic interface on top of the BouncyCastle cryptographic toolkit
 that makes it straightforward to deal with OpenPGP data.
 .
 This package provides CLI command classes for PGPainless-SOP via picocli.

Package: libpgpainless-cli-java-doc
Build-Profiles: <!nodoc>
Architecture: all
Multi-Arch: foreign
Section: doc
Depends:
 ${misc:Depends},
Description: Stateless OpenPGP Protocol with PGPainless - runtime library - docs
 PGPainless is a modern implementation of OpenPGP in Java. It offers an
 ergonomic interface on top of the BouncyCastle cryptographic toolkit
 that makes it straightforward to deal with OpenPGP data.
 .
 This package contains documentation for libpgpainless-cli-java.

Package: pgpainless-cli
Architecture: all
Depends:
 default-jre-headless,
 java-wrappers,
 libpgpainless-cli-java (= ${source:Version}),
 ${maven:Depends},
 ${misc:Depends},
Description: Stateless OpenPGP Protocol with PGPainless - runtime executable
 PGPainless is a modern implementation of OpenPGP in Java. It offers an
 ergonomic interface on top of the BouncyCastle cryptographic toolkit
 that makes it straightforward to deal with OpenPGP data.
 .
 This package provide the command-line interface for the PGPainless Stateless
 OpenPGP Protocol.
